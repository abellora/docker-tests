#!/bin/bash
set -ex

# Install dependencies
pwd
ls /etc/yum.repos.d/
yum -y update && yum clean all
yum -y install python39-devel python39-pip
pip3.9 install numpy

# Install ROOT
yum -y install git make cmake gcc-c++ gcc binutils libX11-devel libXpm-devel libXft-devel libXext-devel openssl-devel patch
SERVICE_USERNAME=ppsgit
git config --global user.name '${SERVICE_USERNAME}'
git config --global user.email '${SERVICE_USERNAME}@cern.ch'
git config --global user.github '${SERVICE_USERNAME}'
cd /home/
git clone --branch v6-28-06 --depth=1 https://github.com/root-project/root.git root_src
mkdir root_build root_install && cd root_build
cmake -DCMAKE_INSTALL_PREFIX=../root_install -DPYTHON_EXECUTABLE=`which python3.9` ../root_src # && check cmake configuration output for warnings or errors
cmake --build . --target install -- -j`nproc` # if you have 4 cores available for compilation